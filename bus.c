/*
Copyright (C) Pr. Olivier Gruber.

This code is part of a suite of educational software,
it is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Classpath; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301 USA.

This is the emulation of the bus subsystem. It emulates not only
the bus itself but also all the devices whose controllers are connected
on the bus.

*/

#include <stddef.h>
#include <stdint.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>

#include "asm.h"
#include "parser.h"
#include <time.h>
#include <fcntl.h>
#include "bus.h"
#include "core.h"
#include <sys/time.h>

extern uint8_t *memory; // physical memory array, allocate globally
extern struct core* core; // registers and all
extern struct bus* bus;

/**
 * This is part of emulating either a serial line or a network card.
 * It provides the ability to connect through a Unix socket domain
 * via a file. This is called from the client side of the unix socket
 * domain in order to establish a connection.
 */
int unix_socket_connect(char * file) {
  struct sockaddr_un addr;
  size_t addr_length;
  int sock;

  sock = socket(PF_UNIX, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("Socket create failed:");
    shutdown(sock,SHUT_RDWR);
    return -1;
  }

  addr.sun_family = AF_UNIX;
  addr_length = sizeof(addr.sun_family) + sprintf(addr.sun_path, "%s", file);
  if (connect(sock, (struct sockaddr *) &addr, addr_length) < 0) {
    // perror("Socket connect failed:");
    shutdown(sock,SHUT_RDWR);
    return -1;
  }

  return sock;
}

/**
 * This is part of emulating either a serial line or a network card.
 * It provides the accept incoming connections.
 * In normal operation, this is blocking until a connection is made.
 */
int unix_socket_accept(int ssock) {
  int sock;
  struct sockaddr_un address;
  socklen_t length = sizeof(address);

  sock = accept(ssock, (struct sockaddr *) &address, &length);
  if (sock == -1 && errno != EAGAIN) {
    perror("unix_socket_accept");
    exit(-1);
  }
  return sock;
}

/**
 * This is part of emulating either a serial line or a network card.
 * It binds a unix socket domain to the given file.
 */
int unix_socket_bind(char * path) {
  int sock;

  printf("unix socket: creating server socket on %s\n", path);
  if ((sock = socket(PF_UNIX, SOCK_STREAM, 0)) < 0) {
    perror("socket() failed");
    exit(666);
  }
  printf("unix socket: socket created : %d\n", sock);

  struct sockaddr_un address;
  unlink(path);
  address.sun_family = AF_UNIX;
  int address_length = sizeof(address.sun_family) +
      sprintf(address.sun_path,"%s",path);

  printf("unix socket: binding %d\n", sock);
  if (bind(sock, (struct sockaddr *) &address, address_length) < 0) {
    perror("bind() failed");
    exit(666);
  }

  if (listen(sock, 5) < 0) {
    perror("listen() failed");
    exit(666);
  }

  return sock;
}

/**
 * This is the actual wrapper of a unix socket domain to emulate
 * a serial line. It waits until a connection is established.
 * Look at the sockcon.c code to see the client side.
 */
void bus_serial_accept() {
  bus->serial.sock= -1;
  bus->serial.ssock= -1;

  bus->serial.ssock = unix_socket_bind(bus->serial.name);
  // fcntl(server, F_SETFL, O_NONBLOCK); // for non-blocking accept
  if (bus->serial.ssock<0) {
    printf("Could not bind to socket=%s\n", bus->serial.name);
    exit(-1);
  }
  printf("Serial Line Emulation:\n");
  printf("  Please start the sockcon program, like this:\n");
  printf("    $ ./sockcon -s:%s \n",bus->serial.name);

  bus->serial.sock = unix_socket_accept(bus->serial.ssock);
  if(bus->serial.sock == -1) {
    printf("Accept failed on socket=%s\n", bus->serial.name);
    exit(-1);
  }
  printf("-> Serial line attached ...\n");
  fcntl(bus->serial.sock, F_SETFL, O_NONBLOCK);
}

/**
 * This is the initialization of the bus, including the
 * initialization of all the devices whose controllers are
 * attached on the bus.
 */
void bus_init(void) {
  strcpy(bus->serial.name,".serial");
  bus->serial.status |= TX_AVAILABLE;
  bus->serial.wdelay = 0;
 
  bus->timer.read_state=1;
  bus->timer.write_state=1;
  bus->timer.lastTimeMeasure=0;
  bus->timer.set=0;

  // You must turn this back on, so that the emulation accept
  // a connection from the sockcon program...
#if 1
  bus_serial_accept();
#endif
}


void  emul_serial_line(){
  
  int count=0;
  char buff[1];

  //Reading part
  count=read(bus->serial.sock,buff,1);
  if(count>0)
  {
    bus->serial.RX= buff[0];
    bus->serial.status|=RX_AVAILABLE;
    raise_interrupt(IRQ_SERIAL);
  }
  else{

    bus->serial.status&=!RX_AVAILABLE;

  } 

  //Sending part
  switch (bus->serial.wdelay){
    case 0 : 
      bus->serial.status|=TX_AVAILABLE;
      break;

    case TX_WDELAY:
      bus->serial.wdelay--;
      bus->serial.status&=!TX_AVAILABLE;
      count= 0;
      buff[0]=(char)(bus->serial.TX);
      while (count<1) {
        count += write(bus->serial.sock,buff,1);
      }
      break;
    default:
      bus->serial.wdelay--;
  }    



}


uint64_t tvalToMilis(struct timeval t)
{
  return (uint64_t)(t.tv_sec*1000) + (uint64_t)(t.tv_usec/1000);

}
/**
 * This is the emulation of devices.
 * It is called after every instruction fetched, decoded, and
 * executed by the core.
 */
void bus_emul_devices(void) {

  //emul serial line
  emul_serial_line();

  //emul timer
  struct timeval t;
  
  if(bus->timer.set){
    gettimeofday(&t,NULL);
    uint64_t newTime= tvalToMilis(t);
    uint64_t timeDiff= newTime - bus->timer.lastTimeMeasure;
    bus->timer.lastTimeMeasure = newTime;

    if( timeDiff >= bus->timer.timer )
    {
      bus->timer.timer=0;
      bus->timer.set=0;
      printf("Timer interrupt\n");
      raise_interrupt(IRQ_TIMER);
    }else
    {
       bus->timer.timer-=timeDiff;
    }
  }

}

/*
 * This is a load operation issued from the CPU on the bus.
 */
uint32_t bus_load(uint32_t addr) {

  // check the validity of the address.
  if (addr >= SIZE_MEM_ARRAY) {
    trap(TRAP_MEMORY_ERROR,addr,0);
    return 0xFFFFFFFF;
  }

  switch (addr){

    case SERIAL_RX: 
      return bus->serial.RX;
      break;

    case SERIAL_STATUS:
      return bus->serial.status;
      break;

    case TIMER_READ:
      if(bus->timer.read_state==0)
      {
        //Low part
        bus->timer.read_state++;     
        // uint32_t ret =    (uint32_t)(bus->timer.timer&0x00000000FFFFFFFF)
        // printf("Low %u\n",ret );
        return (uint32_t)(bus->timer.timer&0x00000000FFFFFFFF);
      }else
      {
        //High part
        bus->timer.read_state=0;
        return (uint32_t)(bus->timer.timer>>32);
      }
      break;

    default :
      return *((uint32_t*) (memory + addr));
  }

  // just a read from physical memory
}




/*
 * This is a store operation issued from the CPU on the bus.
 */
void bus_store(uint32_t addr, uint32_t value) {

  // check the validity of the address.
  if (addr >= SIZE_MEM_ARRAY) {
    trap(TRAP_MEMORY_ERROR,addr,0);
    return;
  }


  switch(addr)
  {
    case SERIAL_TX:

      bus->serial.TX=value;
      bus->serial.wdelay=TX_WDELAY;
      return;


    case TIMER_WRITE:

      if(bus->timer.write_state==0)
      {
        //Low part
        bus->timer.buffer = 0 ;
        bus->timer.buffer |= value ;
        bus->timer.timer= bus->timer.buffer;
        struct timeval t;
        gettimeofday(&t,NULL);
        bus->timer.lastTimeMeasure= tvalToMilis(t);
        bus->timer.write_state++;  
        bus->timer.set=1;
        // printf("Timer is set\n");

      }else
      {
        //High part
        bus->timer.buffer = 0 ;
        bus->timer.buffer = (uint64_t)(value)<<32 ;
        bus->timer.write_state=0;

      }
      break;

    default:
      // just a write to physical memory
      *((uint32_t*) (memory + addr)) = value;

  }

  return;
}
